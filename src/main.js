import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import vuetify from './plugins/vuetify'
import firebase from 'firebase'
import './style/index.css'

Vue.config.productionTip = false

let firebaseConfig = {
  apiKey: "AIzaSyBY0YxBP6fN7lbI6kPrDcbGwx6utaseO6M",
  authDomain: "fir-auth-test-upwork.firebaseapp.com",
  databaseURL: "https://fir-auth-test-upwork.firebaseio.com",
  projectId: "fir-auth-test-upwork",
  storageBucket: "fir-auth-test-upwork.appspot.com",
  messagingSenderId: "512020211063",
  appId: "1:512020211063:web:4b577041a5f22f123aad72"
}

firebase.initializeApp(firebaseConfig)

let app = ''

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      vuetify,
      render: h => h(App)
    }).$mount('#app')
  }
})