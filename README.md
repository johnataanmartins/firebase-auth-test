# Vue front end application with Firebase Authentication

## Firebase auth credentials
````
The setup of Firebase auth credentials is in this file: firebase-auth-test/src/main.js
````

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
